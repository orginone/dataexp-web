import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import path from "path";
import { settings } from './src/config/index';
// https://vitejs.dev/config/
export default defineConfig({
  include: ['src/**'],
  optimizeDeps: { include: [ 'path-to-folder','core-js'  ] },
  plugins: [vue()],
  resolve: {
    // Vite路径别名配置
    alias: {
      '@': path.resolve('./src')
    }
  },
  esbuild: {
    jsxFactory: 'h',
    jsxFragment: 'Fragment',
  },
  build: {
    target: ['es2015', 'chrome63'],
    // ...
    rollupOptions: {
      // ...
      input: {
        main: path.resolve(__dirname, 'index.html'),
       
      }
    }
  },
  server: {
    // 是否主动唤醒浏览器
    open: true,
    // 占用端口
    port: settings.port,
    // 是否使用https请求
    https: settings.https,
    // 扩展访问端口
    host: '0.0.0.0',
    hmr: true,
    watch: {
      usePolling: true // WSL必须,否则热更新无效
    },
    proxy: settings.proxyFlag
      ? {
        '/orginone': {
          // target: 'http://localhost:800', // 后台接口
          target: 'http://orginone.cn:888', // 后台接口
          changeOrigin: true, // 是否允许跨域
          ws: true
        },
      }
      : {}
  }

})
