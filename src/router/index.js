import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path:  '/',
    redirect: '/login'
  },
  {
    path: '/CreativePlatform',
    name: 'CreativePlatform',
    component: ()=> import("@/views/CreativePlatform/index.vue")
  },
  {
    path: '/DesignPlatform',
    name: 'DesignPlatform',
    component: ()=> import("@/views/DesignPlatform/index.vue")
  },
{
    path: '/home',
    name: 'home',
    component: ()=> import("@/views/Home/index.vue")
  },
  {
    path: '/login',
    name: 'login',
    component: () => import("@/views/Login/index.vue")
  },
  {
    path: '/OpenAndSharing', //开放平台
    name: 'OpenAndSharing',
    component: () => import("@/views/OpenAndSharing/index.vue")
  },
  {
    path: '/synergy', //协同平台
    name: 'synergy',
    component: () => import("@/views/Synergy/index.vue")
  },
  {
    path: '/dataDeal', //数据交易
    name: 'dataDeal',
    component: () => import("@/views/DataDeal/index.vue")
  },
  {
    path: '/DataServices',
    name: 'DataServices',
    component: () => import("@/views/DataServices/index.vue")
  }
]

const router = createRouter({
  // process.env.BASE_URL
  history: createWebHistory('/'),
  routes
})

export default router
