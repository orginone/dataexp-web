import {createApp} from 'vue'

import Antd from 'ant-design-vue';
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import './common/common.css'
import 'ant-design-vue/dist/antd.css';

import zhCn from 'element-plus/dist/locale/zh-cn.mjs'

import App from './App.vue'
import router from './router'
import store from './store'

// createApp(App).use(Antd,ElementPlus, {locale: zhCn}).use(ElementPlus).use(store).use(router).mount('#app')
const app = createApp(App);
app.use(Antd);

// 注册 ElementPlus 插件，并设置使用中文语言环境

app.use(ElementPlus, { locale:zhCn });

// 注册 Vuex Store 和 Vue Router
app.use(store);
app.use(router);

// 挂载应用
app.mount('#app');