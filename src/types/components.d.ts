import '@vue/runtime-core'

export {}

declare module '@vue/runtime-core' {
  export interface GlobalComponents {
    Banner2: typeof import('./src/components/Banner2/index.vue')['default']
    Footer: typeof import('./src/components/Footer/index.vue')['default']
    Header: typeof import('./src/components/Header/header.vue')['default']
  }
}
